import 'package:flutter/cupertino.dart';
import 'package:qr_scanner/main.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
    new PreferenciasUsuario._internal();

  factory PreferenciasUsuario(){
    return _instancia;
  } 

  PreferenciasUsuario._internal();

  SharedPreferences prefs;

  initPrefs() async {
    WidgetsFlutterBinding.ensureInitialized();
    this.prefs = await SharedPreferences.getInstance();
  }

  get qr{
    return prefs.getString('qr') ?? '';
  }

  set qr(String value){
    prefs.setString('qr', value);
  }


}