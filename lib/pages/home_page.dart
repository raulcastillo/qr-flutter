import 'package:flutter/material.dart';
import 'package:qr_scanner/pages/direcciones_page.dart';
import 'package:qr_scanner/pages/mapas_page.dart';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:qr_scanner/pages/resultado_page.dart';
import 'package:qr_scanner/preferences/user_preferences.dart';

PreferenciasUsuario prefs = PreferenciasUsuario(); 

class Home extends StatefulWidget{
  _Home  createState() => _Home();
}

class _Home extends State<Home>{

  int currentIndex = 0;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('qr scanner'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete_forever),
          ),
        ],
      ),
      body: _callPage(currentIndex),
      bottomNavigationBar: _crearBottomNavigationBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        child: Icon( Icons.filter_center_focus),
        onPressed: _scanQR,
        backgroundColor: Theme.of(context).primaryColor,
      ),
    );
  }

  _scanQR() async{
    ScanResult futureString;
    try{
      futureString =await BarcodeScanner.scan();
    }catch(e){
      print(e.toString());
    }
    
    prefs.qr = (futureString.rawContent).toString();
    print('future string :' + futureString.rawContent);
    Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (BuildContext context) => ResultPage()));
 
    if(futureString!=null){
      print('teenemosinformacion');
    }
  }

  Widget _callPage(int paginaActual){

    switch(paginaActual){
      case 0: return MapasPage();
      case 1: return DireccionesPage();

      default:
        return MapasPage();
    }
  }

  Widget _crearBottomNavigationBar(){
    return BottomNavigationBar(
      currentIndex: currentIndex,
      onTap: (index){
        setState(() {
          currentIndex = index;
        });
      },
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.map),
          title: Text('mapas')
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.contact_mail),
          title: Text('direcciones')
        ),
      ]
    );
  }
}