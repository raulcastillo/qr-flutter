import 'package:flutter/material.dart';
import 'package:qr_scanner/pages/home_page.dart';
import 'package:qr_scanner/preferences/user_preferences.dart';

final prefs = new PreferenciasUsuario();
void main() async 
{
   await prefs.initPrefs();
   runApp(MyApp());
}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      initialRoute: 'Home',
      routes: {
        'Home' : (BuildContext context) => Home()
      },
      theme: ThemeData(
        primaryColor: Colors.deepPurple
      ),
    );
  }
}